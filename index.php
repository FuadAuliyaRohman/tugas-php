<?php
	include 'db/koneksi.php';
	$query = mysqli_query($link, "SELECT * FROM tbartikel ORDER BY id_artikel DESC");
	$i=0;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">  
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1" />    
    <title>TechTrend</title>
	<!-- Favicon -->
	<link rel="shortcut icon" href="lib/images/favicon.png">
    <meta name="description" content="Simple - Responsive Landing Page Template">
    <meta name="author" content="Simple - Responsive Landing Page Template">
        
	<!-- 1140px Grid styles for IE -->
	<!--[if lte IE 9]><link rel="stylesheet" href="lib/css/ie.css" type="text/css" media="screen" /><![endif]-->

    <!-- CSS concatenated and minified via ant build script-->
    <link rel="stylesheet" href="lib/css/style.css">
    <link rel="stylesheet" href="lib/css/fonts.css">
    <link rel="stylesheet" href="lib/css/animate.css">
    <link rel="stylesheet" href="lib/css/layerslider.css">
    <link rel="stylesheet" href="lib/css/owl.carousel.css">
    <link rel="stylesheet" href="lib/css/owl.theme.css">
    <link rel="stylesheet" href="lib/js/prettyPhoto/css/prettyPhoto.css">
    
    <link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,300italic,400italic' rel='stylesheet' type='text/css'>
    <!-- end CSS-->
    
    <script src="lib/js/jquery-1.11.1.min.js"></script>
    <script src="lib/js/jquery-migrate-1.2.1.js"></script>
    
</head>

<body>

    <!--begin container -->
    <div id="container">
         
        <!--begin header_wrapper -->
        <header id="header_wrapper">
            
            <!--begin header -->    
            <div id="header" class="clearfix">
            
                <!--begin row -->
                <div class="row">
                    
                    <!--begin logo -->            
                    <a href="index.html" id="logo">Simpl<span class="blue">é</span></a>
                    <!--end logo -->
    
                    <!--begin nav -->
                    <nav id="navigation" role="navigation">
                        <ul id="nav">
                            <li>
                                <a href="#container">Home</a>
                            </li>
                            <li>
                                <a href="#blog">Blog</a>
                            </li>
                        </ul>
                    </nav>
                    <!--end nav -->
                    
                </div>
                <!--end row -->
                
            </div>
            <!--end header -->
            
        </header>
        <!--end header_wrapper -->
        
        <!--begin home_wrapper -->
        <section id="home_wrapper">
        
            <!--begin home_box -->
            <div class="home_box">
                
                <h2 class="home_title">innovative &amp; modern template, designed with love for apps.</h2>
                                        
                <h3 class="home_subtitle">Design is not just what it looks like. Design is how it works.</h3>
                
                <!--begin success_box -->
                <p class="newsletter_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
                <!--end success_box -->
                
                <!--begin newsletter_form_wrapper -->
                <div class="newsletter_form_wrapper">
                    
                    <!--begin newsletter_form_box -->
                    <div class="newsletter_form_box">
                        
                        <!--begin success_box -->
                        <p class="newsletter_success_box" style="display:none;">We received your message and you'll hear from us soon. Thank You!</p>
                        <!--end success_box -->
                        
                        <!--begin newsletter-form -->
                        <form id="newsletter-form" class="newsletter_form" action="newsletter.php" method="post">
                            <input id="email_newsletter" type="email" name="nf_email" placeholder="Enter Your Email Address" />  
                            <input type="submit" value="GET STARTED!" id="submit-button-newsletter" />
                        </form>
                        <!--end newsletter-form -->
                    
                    </div>
                    <!--end newsletter_form_box -->
        
                </div>
                <!--end newsletter_form_wrapper -->
                
            </div>
            <!--end home_box -->
            
        </section>
        <!--end home_wrapper -->
        
       
            
        <!--begin blog -->
        <section id="blog">
                        
            <!--begin call_to_action_grey_wrapper -->
            <div class="call_to_action_grey_wrapper">
                
                <!--begin call_to_action_white -->
                <div class="call_to_action_grey">
                    <h3 class="left_fade">News &amp; Announcements</h3>
                    <div class="separator_wrapper bounce_fade">
                        <div class="separator_first_circle">
                            <div class="separator_second_circle">
                            </div>
                        </div>
                    </div>
                    <h4 class="right_fade">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration, by injected humour, or new randomised words which don't look believable.</h4>
                </div>
                <!--end call_to_action_white -->
                    
            </div>
            <!--end call_to_action_grey_wrapper -->
        
            <!--begin section_wrapper -->
            <div class="section_wrapper grey_bg">
                
                <!--begin section_box -->
                <div class="section_box small_margins">
                  
                    <!--begin row -->
                    <div class="row up_fade">
						<?php while ($data=mysqli_fetch_array($query)) {
							
							$id_user=$data['user_id'];
							$id_kategori=$data['kategori_id'];
							$query_user = mysqli_query($link, "SELECT * FROM tbuser where id_user='$id_user'");
							$data_user = mysqli_fetch_array($query_user);
							
							$query_kategori = mysqli_query($link, "SELECT * FROM tbkategori where id_kategori='$id_kategori'");
							$data_kategori = mysqli_fetch_array($query_kategori);
							$i++;
							if($i%3==0){
								$last='last';
							}else{
								$last='';
							}
						?>
							<!--begin fourcol -->
								<div class="fourcol <?php echo $last?>">
									<!--begin blog_item -->
									<div class="blog_item">
											
										<!--begin zoom_photo -->
										<div class="zoom_photo">
																	
											<div class="view view-first">
												<a href="assets/image/<?php echo $data['gambar']?>" class="prettyPhoto[gallery1]">
													<img src="assets/image/<?php echo $data['gambar']?>" alt="pic" style="width:100%;">
													<span class="mask">
														<span class="zoom"></span>
													</span>
												</a>
											</div>
												
										</div>
										<!--end zoom_photo -->
										
										<!--begin blog_item_inner -->
										<div class="blog_item_inner">
											
											<?php if(strlen($data['judul'])>=25){?>
												<h3 class="blog_title"><a href="#"><?php echo mb_substr($data['judul'],0,25)."...";?></a></h3>
											<?php }else{?>
												<h3 class="blog_title"><a href="#"><?php echo $data['judul']?></a></h3>
											<?php }?>
											
											<a href="#" class="blog_icons"><i class="icon icon-user"></i> By <?php echo $data_user[2]?></a>
											
											<a href="#" class="blog_icons last"><i class="icon icon-tags"></i> <?php echo $data_kategori[1]?></a>
											
											<?php if(strlen($data['isi'])>=200){?>
												<p><?php echo mb_substr($data['isi'],0,200).' . . . '?></p>
											<?php }else{?>
												<div style="min-height:120px;">
													<p><?php echo $data['isi']?></p>
												</div>
											<?php }?>
				
											<a href="#" class="button_grey">Read More</a>
											<!--
											<a href="#" class="blog_comments_icon"><i class="icon icon-chat-bubble"></i> 8</a>
											-->		
										</div>
										<!--end blog_item_inner -->
										
									</div>
									<!--end blog_item -->
									
								</div>
                        <!--end fourcol -->
						<?php } ?>
                        <!--begin fourcol -->
                     
                    
                    </div>
                    <!--end row -->
                    
                    
                </div>
                <!--end section_box -->
                
            </div>
            <!--end section_wrapper -->
            
        </section>
        <!--end blog -->
 
        <!--end contact -->
                 
        <!--begin footer -->
        <footer id="footer">
            
            <!--begin footer_box -->
            <div id="footer_box">
                
                <p class="up_fade">Copyright © 2014 Simplé. Designed and Developed by <a href="http://themeforest.net/user/bogdan_09/portfolio?ref=bogdan_09" target="_blank" class="blue">Bogdan</a>.</p>
                
            </div>
            <!--end footer_box -->
        
        </footer>
        <!--end footer -->
           
    </div>
    <!--! end of #container -->
    
    <!-- scripts concatenated and minified via ant build script-->
    <script src="lib/js/modernizr-2.6.1.min.js"></script>
    <script src="lib/js/plugins.js"></script>
    <script src="lib/js/prettyPhoto/js/jquery.prettyPhoto.js"></script>
    <script src="lib/js/retina.js"></script>
    <script src="lib/js/script.js"></script>
    <script src="lib/js/jquery.inview.js"></script>
    
    <!-- begin jquery.sticky script-->
    <script type="text/javascript" src="lib/js/jquery.sticky.js"></script>
		<script>
        $(window).load(function(){
          $("#header_wrapper").sticky({ topSpacing: 0 });
        });
    </script>
    <!-- end jquery.sticky script-->
  
    <!-- begin menu scrollTo script-->
    <script src="lib/js/jquery.scrollTo-min.js"></script>
    <script type="text/javascript">
		(function($){
			$(document).ready(function() {	
			/* This code is executed after the DOM has been completely loaded */
				
				$("#nav li a, a.scrool").click(function(e){
		
					var full_url = this.href;
					var parts = full_url.split("#");
					var trgt = parts[1];
					var target_offset = $("#"+trgt).offset();
					var target_top = target_offset.top;
					
					$('html,body').animate({scrollTop:target_top -69}, 1000);
						return false;
					
				});
						
			});
		})(jQuery);
    </script>
	<!-- end menu scrollTo script-->
    
    <!-- begin menu color script-->
    <script src="lib/js/jquery.nav.js"></script>
	<script>
	$(document).ready(function() {
		$('#nav').onePageNav({
		filter: ':not(.external)',
		scrollThreshold: 0.25
		});
	});
	</script>
    <!-- end menu color script-->
    
    <!--begin shrink header script -->
    <script>
	$(function(){
	 var shrinkHeader = 70;
	  $(window).scroll(function() {
		var scroll = getCurrentScroll();
		  if ( scroll >= shrinkHeader ) {
			   $('#header_wrapper').addClass('shrink');
			}
			else {
				$('#header_wrapper').removeClass('shrink');
			}
	  });
	function getCurrentScroll() {
		return window.pageYOffset;
		}
	});
	</script>
    <!--end shrink header script -->
    
    <!-- begin owl.carousel script-->
    <script src="lib/js/owl.carousel.js"></script>
    <script>
    $(document).ready(function($) {
      $("#owl-example").owlCarousel();
    });
    $("body").data("page", "frontpage");
    </script>
    <!-- end owl.carousel script-->
    
    <!-- begin slider script-->
    <script src="lib/js/responsiveslides.min.js"></script>
  	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
	
		  // Slideshow 4
		  $("#slider4, #slider3").responsiveSlides({
			auto: true,
			pager: true,
			nav: true,
			pause: true,
			speed: 500,
			timeout: 5000,
			namespace: "callbacks",
			before: function () {
			  $('.events').append("<li>before event fired.</li>");
			},
			after: function () {
			  $('.events').append("<li>after event fired.</li>");
			}
		  });
	
		});
  	</script>
    <!-- end slider script-->
    
    <!-- begin layerslider script-->
    <script src="lib/js/greensock.js"></script>
    <script src="lib/js/layerslider.transitions.js"></script>
    <script src="lib/js/layerslider.kreaturamedia.jquery.js"></script>
        
    <script type="text/javascript">
     
        // Running the code when the document is ready
        $(document).ready(function(){
     
            // Calling LayerSlider on the target element
            $('#layerslider2').layerSlider({
     
                autoStart: true,
				pauseOnHover: true,
				navPrevNext: false,
				navButtons: true,
				thumbnailNavigation: false,
			    autoPlayVideos: false,
				firstLayer: 1,
				skin: 'borderlesslight',
				skinsPath: 'lib/layerslider/skins/'
	 
				// Please make sure that you didn't forget to add a comma to the line endings
				// except the last line!
            });
        });
     
    </script>
    <!-- end layerslider script-->

</body>
</html>