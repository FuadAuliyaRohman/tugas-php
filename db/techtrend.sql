-- Adminer 4.6.2 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tbartikel`;
CREATE TABLE `tbartikel` (
  `id_artikel` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `isi` text NOT NULL,
  `tanggal_pembuatan` datetime NOT NULL,
  `gambar` varchar(200) NOT NULL,
  `user_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  PRIMARY KEY (`id_artikel`),
  KEY `user_id` (`user_id`),
  KEY `kategori_id` (`kategori_id`),
  CONSTRAINT `tbartikel_ibfk_5` FOREIGN KEY (`kategori_id`) REFERENCES `tbkategori` (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbartikel` (`id_artikel`, `judul`, `isi`, `tanggal_pembuatan`, `gambar`, `user_id`, `kategori_id`) VALUES
(19,	'kklklkl',	'<p>asasas</p>\r\n',	'2018-05-15 21:48:46',	'cina.png',	3,	2),
(20,	'Kalah dari Home United, Persija Gagal Tembus Final Piala AFC 2018',	'<p>KOMPAS.com&nbsp;-&nbsp; Persija Jakarta gagal lolos ke final Zona ASEAN Piala AFC 2018.&nbsp;Tim berjulukan Macan Kemayoran itu takluk 1-3 pada pertandingan kedua semifinal Zona ASEAN Piala AFC 2018 di Stadion Utama Gelora Bung Karno ( SUGBK), Selasa (15/5/2018). Alhasil, secara agregat, Persija kalah dengan skor 3-6 dari Home United. Pada laga pertama yang digelar di Stadion Jalan Besar, Singapura, Selasa (8/5/2018), Persija menelan kekalahan 2-3. Pada laga ini, dua pemain Persija, yakni Jaimerson dan kiper cadangan Daryono, mendapat kartu merah pada babak pertama. Laga berlangsung dalam intensitas tinggi. Betapa tidak, tiga gol tercipta hanya dalam waktu 12 menit.&nbsp; Sebagai tuan rumah, Persija tertinggal lebih dulu saat laga baru berjalan enam menit.&nbsp; Gol pertama Home United yang diciptakan Shahril Ishak tidak terlepas dari kesalahan kiper Rizky Darmawan.&nbsp; Kiper bernomor punggung 30 tersebut tidak sempurna menepis bola dari umpan yang dilepaskan lawan.&nbsp; Bola liar kemudian berhasil disambut Shahril dengan sepakan keras yang berujung gol.</p>\r\n',	'2018-05-15 22:48:11',	'bola.jpg',	3,	2),
(21,	'Dua Seri Prosesor â€œKaby Lake Xâ€ Intel Pensiun Dini',	'<p>KOMPAS.com - Pada Juni 2017, Intel memperkenalkan duo prosesor PC desktop Core i5-7640X dan Core i7-7740X dari seri &quot;Kaby Lake X&quot; . Kedua prosesor itu jadi alternatif lebih murah seri &quot;Skylake X&quot; (Core i7-7800X, Core i7-7820X, dan Core i9-7900X) yang diumumkan pada saat bersamaan. Prosesor quad-core Kaby Lake X ini diharapkan mampu memancing konsumen agar mau memakai platform High-End Desktop (HEDT) Intel. Sayang, kiprah Core i5-7640X dan Core i7-7740X hanya seumur jagung. Pekan ini, lewat sebuah dokumen, Intel mengatakan Core i5-7640X dan Core i7-7740X segera memasuki masa End of Life (EoL). Dengan kata lain, keduanya menerima pensiun dini, dalam waktu kurang dari setahun setelah melakukan debut di pasaran. Intel akan mulai menghapus Core i5-7640X dan Core i7-7740X dari lini produknya di pasaran mulai 7 Mei mendatang, untuk versi retail dalam box maupun OEM (tray).</p>\r\n',	'2018-05-15 23:16:30',	'intel.jpg',	3,	2);

DROP TABLE IF EXISTS `tbkategori`;
CREATE TABLE `tbkategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(60) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbkategori` (`id_kategori`, `nama_kategori`, `deskripsi`) VALUES
(2,	'Aksesoris Komputer',	'-');

DROP TABLE IF EXISTS `tbuser`;
CREATE TABLE `tbuser` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `nama_lengkap` varchar(60) NOT NULL,
  `emai` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `level` enum('admin','user') NOT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `tbuser` (`id_user`, `username`, `nama_lengkap`, `emai`, `password`, `level`) VALUES
(3,	'admin',	'Administrator',	'admin@admin.com',	'21232f297a57a5a743894a0e4a801fc3',	'admin'),
(4,	'users',	'Users',	'users@users.com',	'9bc65c2abec141778ffaa729489f3e87',	'user');

-- 2018-05-15 16:45:43
