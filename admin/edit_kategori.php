<?php
	session_start();
	if(!isset($_SESSION['nama'])){
		echo "<script> alert('Silahkan login terlebih dahulu'); </script>";
		echo "<meta http-equiv='refresh' content='0; url=../login.php'>";
	}else{

    include '../db/koneksi.php';
    
    $id = isset($_GET['id']) ? $_GET['id'] : "";
    $query = mysqli_query($link, "SELECT * FROM tbkategori WHERE id_kategori='$id'");
		$data = mysqli_fetch_array($query);

    
?>
	<!-- ini untuk konten -->
	<div class="content-wrapper">

	<section class="content-header">
		<h1>
			<small></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-book"></i>Dashboard</a></li>
			<li class="active">Edit Kategori</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
		
		<?php $status = isset($_GET['status']) ?  $_GET['status']  : ""; ?>
			
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Data Kategori</h3>
				</div>

				<form action="?page=update_kategori" method="POST" enctype="multipart/form-data">
					<div class="box-body">
					<?php
						if ($status){
					?>

					<div class="alert alert-danger alert-dismissible">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-close">Gagal!</i></h4>

						<?php echo $status; ?>
					</div>
					<?php
					}
					?>
					<input type="text" name="id"  value="<?php echo $data['id_kategori'] ?>" hidden>
					<div class="box-body">
						<div class="col-md-10">
							<label>Nama Kategori</label>
							<input type="text" class="form-control" placeholder="nama" name="nama" required  value="<?php echo $data['nama_kategori'] ?>">
						</div>
					</div>
					<div class="box-body">
						<div class="col-md-10">
							<label>Deskripsi</label>
							<textarea class="form-control" name="deskripsi"><?=$data['deskripsi']?></textarea>
						</div>
					</div>
					
					<div class="box-body">
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="Update" name="update">
							<a href="javascript:history.back()" class="btn btn-danger">Kembali</a>
						</div>
					</div>						
				</form>

			</div>
		</div>	
		</div>
	</section>
</div>

<?php
}
?>