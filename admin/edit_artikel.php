<?php
	if(!isset($_SESSION['nama'])){
		echo "<script> alert('Silahkan login terlebih dahulu'); </script>";
		echo "<meta http-equiv='refresh' content='0; url=../login.php'>";
	}else{

    include '../db/koneksi.php';
    
    $id = isset($_GET['id']) ? $_GET['id'] : "";
    $query = mysqli_query($link, "SELECT * FROM tbartikel WHERE id_artikel='$id'");
		$data = mysqli_fetch_array($query);
	
	$query_kategori = mysqli_query($link, "SELECT * FROM tbkategori");
    
?>
	<!-- ini untuk konten -->
	<div class="content-wrapper">

	<section class="content-header">
		<h1>
			<small></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-book"></i>Dashboard</a></li>
			<li class="active">Edit Artikel</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
		
		<?php $status = isset($_GET['status']) ?  $_GET['status']  : ""; ?>
			
		<div class="col-md-12">
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Edit Data Artikel</h3>
				</div>

				<form action="?page=update_artikel" method="POST" enctype="multipart/form-data">
					<div class="box-body">
					<?php
						if ($status){
					?>

					<div class="alert alert-danger alert-dismissible">
						<button class="close" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
						<h4><i class="icon fa fa-close">Gagal!</i></h4>

						<?php echo $status; ?>
					</div>
					<?php
					}
					?>
					<input type="text" name="id"  value="<?php echo $data['id_artikel'] ?>" hidden>
					<div class="box-body">
						<div class="col-md-10">
							<label>Kategori</label>
							<select class="form-control" name="kategori_id" required>
								<option value="">- Pilh Kategori -</option>
								<?php 
									while($row=mysqli_fetch_assoc($query_kategori)){
										if($row['id_kategori']==$data['kategori_id']){
											echo "<option value=' $row[id_kategori]' selected>$row[nama_kategori]</option>";
										}else{
											echo "<option value=' $row[id_kategori]'>$row[nama_kategori]</option>";
										}
									}
								?>
							</select>
						</div>
					</div>
					
					<div class="box-body">
						<div class="col-md-10">
							<label>Judul</label>
							<input type="text" class="form-control" placeholder="judul" name="judul" value="<?=$data['judul']?>" required>
						</div>
					</div>


					<div class="box-body">
						<div class="col-md-10">
							<label>Isi</label>
							<textarea class="form-control" name="isi" rows="6" cols="6"><?php echo $data['isi']?></textarea>
						</div>
					</div>
					
					<div class="box-body">
						<div class="col-md-10">
							<label>Gambar</label>
							<input type="file" class="form-control" name="gambar">
							<small style="color:red;">Ukuran Gambar Harus 750 x 500</small>
						</div>
					</div>
					
					<div class="box-body">
						<div class="box-footer">
							<input type="submit" class="btn btn-primary" value="Update" name="update">
							<a href="javascript:history.back()" class="btn btn-danger">Kembali</a>
						</div>
					</div>						
				</form>

			</div>
		</div>	
		</div>
	</section>
</div>
<script src="../assets/ckeditor/ckeditor.js"></script>
<script src="../assets/js/jquery-2.2.3.min.js"></script>
<script>
	$(function(){
		CKEDITOR.replace('isi')
	})
</script>
<?php
}
?>