<?php
	session_start();
	if(!isset($_SESSION['nama'])){
		echo "<script> alert('Silahkan login terlebih dahulu'); </script>";
		echo "<meta http-equiv='refresh' content='0; url=../login.php'>";
	}else{
?>
	<!-- ini untuk konten -->
	<div class="content-wrapper">

	<section class="content-header">
		<h1>
			<small></small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-book"></i>Dashboard</a></li>
			<li class="active">Daftar Kategori</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div>
					<div class="box box-primary" style="padding:20px;">
						<div class="box-header with-border">
							<h3 class="box-title">Daftar Kategori</h3>
							<a href="?page=tambah_kategori" class="btn btn-primary pull-right">Tambah Kategori</a>
						</div>
						<hr/>
						<table class="table table-bordered">
							<?php
								$halaman = 5;
								$page = isset($_GET["halaman"]) ? (int)$_GET["halaman"] : 1;
								$mulai = ($page>1) ? ($page * $halaman) - $halaman : 0;
								$result = mysqli_query($link, "SELECT * FROM tbkategori");

								$total = mysqli_num_rows($result);
								$pages = ceil($total/$halaman);
								$query = mysqli_query($link, "SELECT * FROM tbkategori LIMIT $mulai, $halaman") or die(mysqli_error($link));

							?>
							<thead>
								<tr>
									<th style="width:50px;">No</th>
									<th>Nama</th>
									<th>Deskripsi</th>
									<th class="text-center" style="width:100px;">Aksi</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$i=1;
									while ($data=mysqli_fetch_array($query)) {
										?>
										<tr>
											<td class="text-center"><?php echo $i++; ?></td>
											<td><?php echo $data[1]; ?></td>
											<td><?php echo $data[2]; ?></td>
											<td class="text-center">
												<a class="btn btn-sm btn-success" href="?page=edit_kategori&id=<?php echo $data[0]; ?>"><i class="fa fa-edit"></i></a>
												<a class="btn btn-sm btn-danger" href="?page=hapus_kategori&id=<?php echo $data[0]; ?>" onclick="return confirm('Anda yakin ingin menghapus Kategori <?php echo $user[1]; ?> ?')"><i class="fa fa-trash"></i></a>
											</td>
										</tr>
								<?php
									}
								?>
							</tbody>
							
						</table>
						<div class="box-footer clearfix">
							<ul class="pagination pagination-sm no-margin pull-right">
							<li><a href="#">Halaman</a></li>
								<?php for($i=1; $i<=$pages; $i++) { ?>
									<li><a href="?page=list_user&halaman=<?php echo $i; ?>"><?php echo $i; ?></a></li>
								<?php } ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

		
	</div>
<?php
}
?>